/** window.h
 **
 ** Copyright 2022 Salim Monder
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#pragma once

#include <memory>
#include <gtkmm.h>

namespace AppTemp
{
  class Window : public Gtk::ApplicationWindow
  {
    public:
      // CONSTRUCTORS
      /* Due to convention of using Gtk::Builder::get_widget_derived()
       * constructor of the class should look like this. You can read
       * more about it in the reference.
       */
      Window (BaseObjectType * cobject,
              const Glib::RefPtr<Gtk::Builder>& refBuilder);

      static std::unique_ptr<Window> create();

      //DESTRUCTORS

    private:
      Glib::RefPtr<Gtk::Builder> m_refBuilder;
      Gtk::Label* m_pLabel;
  };
}
