/** application.hpp
 ** This file is part of the AdwAppTemplate project.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#pragma once

#include <adwaita.h>
#include <gtkmm.h>
#include <memory>


namespace AppTemp
{
  class Application : public Gtk::Application
  {
    public:
      // CONSTRUCTORS
      Application (const Application &);
      Application (const Glib::ustring &   app_id = {},
                   Gio::Application::Flags flags = Gio::Application::Flags::NONE);

      static std::shared_ptr<Application> create();

      // GETTERS
      AdwStyleManager * StyleManager() const;

    protected:
      // APPLICATION CLASS OVERRIDES
      void on_startup() override;
      void on_activate() override;

    private:
      AdwStyleManager * m_StyleManager;
  };
}
