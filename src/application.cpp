/** application.cpp
 ** This file is part of the AdwAppTemplate project.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as
 ** published by the Free Software Foundation, either version 3 of the
 ** License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#define G_LOG_DOMAIN "Application"

#include <iostream>
#include <memory>

#include "application.hpp"
#include "window.hpp"

namespace AppTemp
{

  Application::Application (const Application &) : Gtk::Application()
  {}

  Application::Application(const Glib::ustring & app_id, Flags flags)
  {}

  std::shared_ptr<Application> Application::create ()
  {
    std::shared_ptr<Application> app =
        std::make_shared<Application> ("io.Foo.AdwTemplate",
                                       Gio::Application::Flags::NONE);

    if (!app)
        throw std::runtime_error("Failed to create application.");

    return app;
  }

  void Application::on_startup ()
  {
    Gio::Application::on_startup ();

    adw_init ();

    m_StyleManager = adw_style_manager_get_default();
  }

  void Application::on_activate ()
  {
    static std::unique_ptr <Window> window;

    if (! window)
      {
        window = Window::create();

        add_window (*window);
      }

    window->present();
  }

  AdwStyleManager * Application::StyleManager() const
  {
    return m_StyleManager;
  }

}
