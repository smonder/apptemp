/** main.cpp
 **
 ** Copyright 2022 Salim Monder
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <memory>

#include "application.hpp"

int
main (int argc, char *argv[])
{
  int ret;

  /* Create a new Gtk::Application. The application manages our main loop,
   * application windows, integration with the window manager/compositor, and
   * desktop features such as file opening and single-instance applications.
   */
  Glib::RefPtr<AppTemp::Application> app =
      AppTemp::Application::create ();

  std::locale::global(std::locale("C"));

  /* Run the application. This function will block until the application
   * exits. Upon return, we have our exit code to return to the shell. (This
   * is the code you see when you do `echo $?` after running a command in a
   * terminal.
   */
  ret = app->run(argc, argv);

  return ret;
}
