/** apptemp-window.cpp
 **
 ** Copyright 2022 Salim Monder
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "window.hpp"

namespace AppTemp
{

  Window::Window (BaseObjectType* cobject,
                  const Glib::RefPtr<Gtk::Builder>& refBuilder)
    : Gtk::ApplicationWindow(cobject)
    , m_refBuilder(refBuilder)
    , m_pLabel(nullptr)
  {
    /* Just an example of getting widgets in derived ApplicationWindow constructor.
     * You have to declare pointer in header file of needed type and initialize a
     * member pointer to nullptr in constructor initializer list. But you have to
     * do it only for widgets that you want to access or change from code.
     */
    m_pLabel = m_refBuilder->get_widget<Gtk::Label> ("label");
  }

  std::unique_ptr<Window> Window::create()
  {
    // Parse a resource file containing a GtkBuilder UI definition.
    auto builder = Gtk::Builder::create_from_resource("/io/example/temp/window.ui");

    /* Get ApplicationWindow that is specified in the UI file but
     * implemented in our code. So our ApplicationWindow is derived.
     */
    Window* window = nullptr;
    window = Gtk::Builder::get_widget_derived<AppTemp::Window> (builder, "window");

    if (!window)
      throw std::runtime_error("No \"window\" object apptemp-window.ui");

    return std::unique_ptr<Window>(window);
  }

}
